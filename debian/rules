#!/usr/bin/make -f
# -*- makefile -*-
# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

# Set POD_MAN_DATE to build manpages reproducible with the timestamp
# of the last changelog entry ...  The input for pod2man is generated from a
# template at build-time, resetting the timestamps
SOURCE_DATE_EPOCH ?= $(shell date -d "$$(dpkg-parsechangelog -SDate)" +%s)
export POD_MAN_DATE = $(shell date -u +"%Y-%m-%d" --date="@$(SOURCE_DATE_EPOCH)")

override_dh_auto_configure:
	pkghome=/usr/share \
	varprefix=/var/lib/torrus \
	cachedir=/var/cache/torrus \
	piddir=/var/run/torrus \
	logdir=/var/log/torrus \
	pkgbindir=/usr/share/torrus/bin \
	wrapperdir=/usr/sbin \
	cfgdefdir=/usr/share/torrus/conf_defaults \
	pkgdocdir=/usr/share/doc/torrus-common \
	exmpdir=/usr/share/doc/torrus-common/examples \
	perllibdir=/usr/share/perl5 \
	pluginsdir=/usr/share/torrus/plugins \
	scriptsdir=/usr/share/torrus/scripts \
	supdir=/usr/share/torrus/sup \
	tmpldir=/usr/share/torrus/templates \
	distxmldir=/usr/share/torrus/xmlconfig \
	defrrddir=/var/lib/torrus/collector_rrd \
	mansec_usercmd=8 \
	torrus_user=Debian-torrus \
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--datadir=/usr/share/torrus \
		--sharedstatedir=/var/lib/torrus \
		--localstatedir=/var/lib/torrus \
		--libdir=/usr/share/torrus/lib \
		--mandir=/usr/share/man \
		--enable-pkgonly \
		--disable-varperm \
		SHELL=/bin/sh \
		CONFIG_SHELL=/bin/sh


override_dh_auto_build:
	dh_auto_build
	cd debian/html && $(MAKE)


override_dh_install:
	# Rename example script, dunno why
	mv debian/torrus-common/usr/share/torrus/scripts/rrdup_notify.sh \
		debian/torrus-common/usr/share/torrus/scripts/rrdup_notify
	chmod a+x debian/torrus-common/usr/share/torrus/scripts/rrdup_notify

	# /var/run is created by init script
	rmdir debian/torrus-common/var/run/torrus debian/torrus-common/var/run

	# Move examples, Bug #579872
	mv debian/torrus-common/usr/share/torrus/xmlconfig/examples/* debian/torrus-common/usr/share/doc/torrus-common/examples
	rmdir debian/torrus-common/usr/share/torrus/xmlconfig/examples
	dh_install


override_dh_installdocs:
	dh_installdocs --link=torrus-common

override_dh_installinit:
	# install init script
	mkdir -p debian/torrus-common/etc/init.d
	cp init.d/torrus debian/torrus-common/etc/init.d/torrus-common
	dh_installinit --onlyscripts

override_dh_auto_clean:
	dh_auto_clean
	cd debian/html && $(MAKE) clean


%:
	dh $@
